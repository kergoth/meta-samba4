SUMMARY = "Heimdal is a Kerberos 5 implementation"
DESCRIPTION = "Heimdal is an implementation of Kerberos 5 that aims to be \
protocol compatible with existing implementations and RFC 4120. It supports \
Kerberos V5 over GSS-API (RFC 1964) and PK-INIT (smartcard support) for \
Kerberos, and includes a number of important and useful applications (rsh, \
telnet, popper, etc.). Heimdal also contains an ASN.1 compiler, X.509 library, \
and NTLM (v1 and v2) library."
HOMEPAGE = "http://www.h5l.org/"
DEPENDS += "bison-native virtual/libintl"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d2c6f8cfe82d4fdd74355866f0c14d3f"

PR = "r0"
SRC_URI = "http://www.h5l.org/dist/src/heimdal-1.5.3.tar.gz"
SRC_URI[md5sum] = "30b379e3de12f332fbd201131f02ffca"
SRC_URI[sha256sum] = "aac27bedb33c341b6aed202af07ccc816146a893148721f8123abbbf93bbfea5"

inherit autotools

# --disable-pk-init
# --disable-digest
# --disable-kx509
# --enable-dce
# --disable-afs-support
# --disable-mmap
# --disable-afs-string-to-key
# --disable-afs
# --enable-kcm
# --disable-heimdal-documentation
PACKAGECONFIG ?= ""
PACKAGECONFIG[openldap] = "--with-openldap,--without-openldap,openldap"
PACKAGECONFIG[sqlite3] = "--with-sqlite3,--without-sqlite3,sqlite3"
PACKAGECONFIG[openssl] = "--with-openssl,--without-openssl,openssl"
PACKAGECONFIG[berkeley-db] = "--with-berkeley-db,--without-berkeley-db,berkeley-db"
PACKAGECONFIG[readline] = "--with-readline,--without-readline,readline"
PACKAGECONFIG[libedit] = "--with-libedit,--without-libedit,libedit"
PACKAGECONFIG[hesiod] = "--with-hesiod,--without-hesiod,hesiod"
PACKAGECONFIG[x11] = "--with-x,--without-x,virtual/libx11"
PACKAGECONFIG[capng] = "--with-capng,--without-capng,capng"

BBCLASSEXTEND += "native"
