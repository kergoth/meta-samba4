SUMMARY = "Samba is the standard Windows interoperability suite of programs \
for Linux and Unix."
DESCRIPTION = "Samba is a software suite that provides seamless file and print \
services to SMB/CIFS clients. It is freely available, unlike other SMB/CIFS \
implementations, and allows for interoperability between Linux/Unix servers \
and Windows-based clients."
SECTION = "console/network"
LICENSE = "GPL-3.0+"
# FIXME: Add headers from a couple files to incorporate the 'or later version'
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"
# e2fsprogs-native perl?
DEPENDS = "readline e2fsprogs zlib popt libcap python heimdal-native"
# docbook-sgml-dtd 3.1, 4.1, 4.5. which version do we need?
# is docbook-utils-native used?
DEPENDS += "docbook-utils-native docbook-sgml-dtd-3.1-native"

SRC_URI = "https://ftp.samba.org/pub/samba/stable/samba-${PV}.tar.gz \
           file://cache.txt"

# file://volatiles.03_samba
# file://smb.conf
# file://init.samba
# file://init.winbind
# file://tdb.pc

# TODO: split out granular packaging: perl, python, etc. look at debian as
# a starting point and break it down further from there if appropriate
# FIXME: use SOLIBS/SOLIBSDEV
FILES_${PN} += "\
    ${datadir}/samba \
    ${libdir}/samba/lib*.so.* \
    ${libdir}/samba/*/*.so \
    \
    ${libdir}/mit_samba.so \
    ${libdir}/winbind_krb5_locator.so \
    \
    ${PYTHON_SITEPACKAGES_DIR}/*.so \
    ${PYTHON_SITEPACKAGES_DIR}/*.py \
    ${PYTHON_SITEPACKAGES_DIR}/samba/tests/ \
    ${PYTHON_SITEPACKAGES_DIR}/samba/external/ \
    ${PYTHON_SITEPACKAGES_DIR}/samba/*.py \
    ${PYTHON_SITEPACKAGES_DIR}/samba/*.so \
    ${PYTHON_SITEPACKAGES_DIR}/samba/*/*.py \
    ${PYTHON_SITEPACKAGES_DIR}/samba/*/*.so \
    ${datadir}/perl5 \
"
FILES_${PN}-dev += "\
    ${libdir}/samba/lib*.so \
"
FILES_${PN}-dbg += "\
    ${libdir}/samba/.debug \
    ${libdir}/samba/*/.debug \
    ${PYTHON_SITEPACKAGES_DIR}/.debug \
    ${PYTHON_SITEPACKAGES_DIR}/samba/.debug \
    ${PYTHON_SITEPACKAGES_DIR}/samba/*/.debug \
"

S = "${WORKDIR}/samba-${PV}"

inherit waf pythonnative update-rc.d

# For python2.7-config
export BUILD_SYS
export HOST_SYS
export STAGING_INCDIR
export STAGING_LIBDIR

# FIXME: add /run/lock/samba to volatiles, both for sysvinit and systemd

# For asn1_compile
PATH =. "${STAGING_LIBDIR_NATIVE}/heimdal/heimdal:"

PACKAGECONFIG = "\
    ${@base_contains('DISTRO_FEATURES', 'pam', 'pam', '', d)} \
    acl aio avahi gnutls regedit gettext iconv \
"
# pam_smbpass?
PACKAGECONFIG[pam] = "--with-pam,--without-pam,libpam"
PACKAGECONFIG[fam] = "--with-fam,--without-fam,gamin"
#PACKAGECONFIG[talloc] = "--enable-libtalloc,--disable-libtalloc,talloc"
PACKAGECONFIG[acl] = "--with-acl-support,--without-acl-support,acl"
PACKAGECONFIG[aio] = "--with-aio-support,--without-aio-support,libaio"
PACKAGECONFIG[avahi] = "--enable-avahi,--disable-avahi,avahi"
PACKAGECONFIG[gnutls] = "--enable-gnutls,--disable-gnutls,gnutls"
PACKAGECONFIG[regedit] = "--with-regedit,--without-regedit,ncurses"
PACKAGECONFIG[gettext] = "--with-gettext=${STAGING_EXECPREFIXDIR},--without-gettext,gettext"
PACKAGECONFIG[iconv] = "--with-libiconv=${STAGING_EXECPREFIXDIR},--without-libiconv,virtual/libiconv"
# winbind, quotas, sendfile, icon, readline

INITSCRIPT_PACKAGES = "samba winbind"
INITSCRIPT_NAME_samba = "samba"
INITSCRIPT_NAME_winbind = "winbind"
# No dependencies, goes in at level 20 (NOTE: take care with the
# level, later levels put the shutdown later too - see the links
# in rc6.d, the shutdown must precede network shutdown).
INITSCRIPT_PARAMS = "defaults"
CONFFILES_${PN} = "${sysconfdir}/samba/smb.conf"

EXTRA_OECONF += "\
    '--sysconfdir=${sysconfdir}' \
    '--localstatedir=${localstatedir}' \
    '--enable-fhs' \
    \
    --cross-compile \
    --cross-answers=${S}/cache.txt \
    --hostcc=${BUILD_CC} \
    \
    --disable-rpath \
    --disable-rpath-install \
    \
    --disable-cups \
    --disable-iprint \
    --disable-glusterfs \
    --without-dmapi \
    --without-ldap \
    --without-cluster-support \
    --without-ads \
"

do_configure_prepend () {
    cp ${WORKDIR}/cache.txt ${S}/
    echo 'Checking uname machine type: "${TARGET_ARCH}"' >>${B}/cache.txt
    case "${SITEINFO_ENDIANNESS}" in
        le)
            echo 'Checking for HAVE_LITTLE_ENDIAN: OK'
            ;;
        be)
            echo 'Checking for HAVE_BIG_ENDIAN: OK'
            ;;
    esac >>${S}/cache.txt
}

do_install_append () {
    # TODO: install volatiles configs to handle these
    rm -rf ${D}${localstatedir}/run ${D}${localstatedir}/lock \
           ${D}${localstatedir}/log
}
