require recipes-connectivity/samba4/samba4.inc

SRC_URI += "\
    file://samba4-0001-build-don-t-execute-tests-summary.c.patch \
    file://samba4-0002-build-don-t-execute-statfs-and-f_fsid-checks.patch \
    file://samba4-0003-build-find-FILE_OFFSET_BITS-via-array.patch \
    file://samba4-0004-build-allow-some-python-variable-overrides.patch \
    file://samba4-0005-builtin-heimdal-external-tools.patch \
    file://samba4-0007-build-find-blkcnt_t-size-via-array.patch \
    file://samba4-0008-build-unify-and-fix-endian-tests.patch \
    file://samba4-0009-build-make-wafsamba-CHECK_SIZEOF-cross-compile-friendl.patch \
    file://samba4-0010-build-tweak-SIZEOF-utmp-ut_line.patch \
"
SRC_URI[md5sum] = "bc44b0a245468c3574ef07644206ede3"
SRC_URI[sha256sum] = "15a0ccc2fd90166c4231574f4f1a1229769be2cc4da7af9c16304e8659529d89"
