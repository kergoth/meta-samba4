# do_install_append() {
#     install -d ${D}${libdir}/pkgconfig/
#     cp ${WORKDIR}/tdb.pc ${D}${libdir}/pkgconfig/
#     mv ${D}${libdir}/libsmbclient.so ${D}${libdir}/libsmbclient.so.0 || true
#     ln -sf libsmbclient.so.0 ${D}${libdir}/libsmbclient.so
#     mkdir -p ${D}${base_sbindir}
#     rm -f ${D}${bindir}/*.old
#     rm -f ${D}${sbindir}/*.old
#     [ -f ${D}${sbindir}/mount.cifs ] && mv ${D}${sbindir}/mount.cifs ${D}${base_sbindir}/
#     [ -f ${D}${sbindir}/umount.cifs ] && mv ${D}${sbindir}/umount.cifs ${D}${base_sbindir}/

#     # This is needed for < 3.2.4
#     rm -f ${D}${sbindir}/mount.smbfs ${D}${base_sbindir}/mount.smbfs
#     if [ -f ${D}${bindir}/smbmount ]; then
#         ln -sf ${bindir}/smbmount ${D}${base_sbindir}/mount.smb
#         ln -sf ${bindir}/smbmount ${D}${base_sbindir}/mount.smbfs
#     fi
    
#         install -D -m 755 ${WORKDIR}/init.samba ${D}${sysconfdir}/init.d/samba
#         install -D -m 755 ${WORKDIR}/init.winbind ${D}${sysconfdir}/init.d/winbind
#     install -D -m 644 ${WORKDIR}/smb.conf ${D}${sysconfdir}/samba/smb.conf
#     install -D -m 644 ${WORKDIR}/volatiles.03_samba ${D}${sysconfdir}/default/volatiles/volatiles.03_samba
#     install -d ${D}/var/log/samba
#     install -d ${D}/var/spool/samba

#     # Install other stuff not installed by "make install"
#     if [ -d ${WORKDIR}/${PN}-${PV}/nsswitch ]; then
#         install -m 0644 ${WORKDIR}/${PN}-${PV}/nsswitch/libnss_winbind.so ${D}${libdir}/libnss_winbind.so.2
#         install -m 0644 ${WORKDIR}/${PN}-${PV}/nsswitch/libnss_wins.so ${D}${libdir}/libnss_wins.so.2
#     fi

#     rmdir --ignore-fail-on-non-empty ${D}${base_sbindir}
#     sed -i -e '1s,#!.*perl,#!${USRBINPATH}/env perl,' ${D}${bindir}/findsmb

#     # usershare mount place
#     mkdir -p ${D}${localstatedir}/lib/samba/usershares
# }

# pkg_postinst_libnss-winbind () {
#     # add wins to the list of resolvers
#     ns=$D${sysconfdir}/nsswitch.conf
#     if ! grep "hosts:.*wins" $ns > /dev/null; then
#     hosts="`grep '^hosts:' $ns`"
#     hosts=`echo "$hosts" | sed 's/\[/\\\\[/g; s/\]/\\\\]/g'`
#     sed -i "s/$hosts/$hosts wins/" "$ns"
#     fi
# }
