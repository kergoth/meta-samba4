WAF ?= "${S}/buildtools/bin/waf"
CONFIGUREOPTS ?= "'--prefix=${prefix}'"

do_configure () {
    python ${WAF} configure ${CONFIGUREOPTS} ${EXTRA_OECONF}
}

do_compile () {
    python ${WAF} build ${PARALLEL_MAKE}
}

do_install () {
    python ${WAF} install '--destdir=${D}'
}
